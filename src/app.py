import os
import streamlit as st
import scipdf
import json
import requests

out_path = '/tmp/'
url = 'http://coqui-tts:5002'
def synthetize2file(text, file_name, path=out_path, speaker_id='p376'):
    req = requests.get(url + "/api/tts", params={'text': text, 'speaker_id': speaker_id})

    if req.status_code == 200 and req.headers['Content-Type'] == 'audio/wav':
        full_path = os.path.join(path, file_name)
        with open(full_path, 'wb') as f:
            f.write(req.content)
        return full_path
    else:
        st.warning("No audio has been returned from Coqui TTS server api")
        return None


st.title('Pdf to dict')
uploaded_file = st.file_uploader("Choose a pdf", type=["pdf"])

if uploaded_file:
    with open('article.pdf', 'wb') as f:
        for line in uploaded_file:
            f.write(line)
    article_dict = scipdf.parse_pdf_to_dict(
        'article.pdf',
        as_list=False,
        grobid_url='http://grobid:8070'
    )
    with open('article.json', 'w') as json_file:
        json.dump(article_dict, json_file)
    with open('article.json') as f:
        st.download_button('Download article.json', f, file_name="article.json")

    st.header(article_dict['title'])
    st.markdown('## Abstract')
    st.markdown(article_dict['abstract'])

    audio_path = synthetize2file(article_dict['abstract'], 'abstract.wav')
    if audio_path is not None:
        st.audio(audio_path)

    for i, sec in enumerate(article_dict['sections']):
        st.markdown(f'## {sec["heading"]}')
        st.markdown(sec['text'])
        audio_path = synthetize2file(sec['text'], f'{i}.wav')
        if audio_path is not None:
            st.audio(audio_path)
