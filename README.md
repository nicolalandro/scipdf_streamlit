# Sci PDF
A library to read and trasform in string scientific pdf.

![demo](imgs/demo.png)

## How to use
* docker-compose up
* localhost:8501 # streamlit interface for paper pdf reader
* localhost:5002 # coqui tts for text2speach

## References
* [scipdf parser](https://github.com/titipata/scipdf_parser/blob/master/scipdf/pdf/parse_pdf.py)
